CREATE TABLE venda (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	data_venda DATE NOT NULL,
	data_pagamento DATE NOT NULL,
	codigo_vendedor BIGINT(20) NOT NULL,
	codigo_cliente BIGINT(20) NOT NULL,
	valor_venda DECIMAL(10,2) NOT NULL,
	valor_pago DECIMAL(10,2) NOT NULL,
	FOREIGN KEY (codigo_vendedor) REFERENCES vendedor(codigo),
	FOREIGN KEY (codigo_cliente) REFERENCES cliente(codigo)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

