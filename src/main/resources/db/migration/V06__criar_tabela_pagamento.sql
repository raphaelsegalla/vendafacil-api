CREATE TABLE pagamento (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	data_pagamento DATE NOT NULL,
	valor_pagamento DECIMAL(10,2) NOT NULL,
	tipo_pagamento VARCHAR(20) NOT NULL,
	tipo_parcela VARCHAR(20) NOT NULL,
	observacao VARCHAR(200),
	total_pago BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;