CREATE TABLE cliente (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	cpf VARCHAR(20) NOT NULL,
	telefone VARCHAR(30) NOT NULL,
	celular VARCHAR(30) NOT NULL,
	logradouro VARCHAR(100),
	numero VARCHAR(30),
	complemento VARCHAR(30),
	bairro VARCHAR(30),
	cep VARCHAR(30),
	cidade VARCHAR(30),
	estado VARCHAR(30),
	situacao BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO cliente 
	(nome,email,telefone,celular,cpf,logradouro,numero,complemento,bairro,cep,cidade,estado,situacao) 
	values 
	('Alessandra Liberato','aliberatosegalla@email.com','(12) 39020822','(12) 996012265','332.695.178-74', 'Rua João Fonseca', '109', 'apto.303a', 'Brasil', '12230-088', 'São José dos Campo', 'SP', true);
INSERT INTO cliente 
	(nome,email,telefone,celular,cpf,logradouro,numero,complemento,bairro,cep,cidade,estado,situacao) 
	values 
	('Thaina Liberato','thaina.liberato@email.com','(12) 39020822','(12) 945456789','445.678.987-45', 'Av. Andromeda', '450', 'apto.303a', 'Brasil', '12200-000', 'São José dos Campo', 'SP', true);
INSERT INTO cliente 
	(nome,email,telefone,celular,cpf,logradouro,numero,complemento,bairro,cep,cidade,estado,situacao) 
	values 
	('Raphael Fernando Pimentel Segalla','raphael.segalla@gmail.com','(12) 39332027','(12) 996586115','315.595.098-64', 'Rua João Fonseca dos Santos', '109', 'apto.303a', 'Floradas de São José', '12230-088', 'São José dos Campo', 'SP', true);
INSERT INTO cliente 
	(nome,email,telefone,celular,cpf,logradouro,numero,complemento,bairro,cep,cidade,estado,situacao) 
	values 
	('Maria Margarida Pimentel Segalla','maria.margarida@email.com','(12) 39020822','(12) 982397275','006.326.598-23', 'Rua Abilia Machado', '53', 'apto.103', 'Vila Industrial', '12200-000', 'São José dos Campo', 'SP', true);		