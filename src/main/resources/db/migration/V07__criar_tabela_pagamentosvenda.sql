CREATE TABLE pagamentosvenda (
	codigo_venda BIGINT(20) NOT NULL,
	codigo_pagamento BIGINT(20) NOT NULL,
	FOREIGN KEY (codigo_venda) REFERENCES venda(codigo) ON DELETE CASCADE,
	FOREIGN KEY (codigo_pagamento) REFERENCES pagamento(codigo) ON DELETE CASCADE,
	UNIQUE (codigo_venda, codigo_pagamento)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;