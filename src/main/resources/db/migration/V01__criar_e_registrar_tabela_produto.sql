CREATE TABLE produto (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	tipo VARCHAR(20) NOT NULL,
	comprimento DECIMAL(10,2),
	largura DECIMAL(10,2),
	espessura DECIMAL(10,2),
	numero INT,
	peso DECIMAL(10,2),
	data_compra DATE NOT NULL,
	valor_compra DECIMAL(10,2) NOT NULL,
	valor_venda DECIMAL(10,2) NOT NULL,
	observacao VARCHAR(200),
	vendido BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Corrente laminada','CORRENTE',45,3,1,null,5,'2019-06-08',68.72,205,'Teste',true); 
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Anel de tartaruga','ANEL',null,null,null,17,10,'2019-06-08',23.87,70,'Anel envelhecido',true);	
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira 3 em 1 grossa','PULSEIRA',23,5,2,null,17,'2019-06-08',89.34,270,'Pulseira com chanfro',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira de Pandora','PULSEIRA',23,3,3,null,10,'2019-06-08',99.84,300,'Pulseira da Eternity',true);	
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira rabo de rato media','PULSEIRA',23,2,2,null,15,'2019-07-01',37.74,120,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira de bolinha pequena','PULSEIRA',22,1,1,null,7,'2019-07-01',12.47,40,'Bolinhas ocas',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Corrente peruana','CORRENTE',50,15,15,null,120,'2019-07-01',212.74,650,'Pulseira envelhecida',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pingente rabo de baleia','PINGENTE',5,4,1,null,4,'2019-07-01',21.04,65,'Lisa',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Tornozeleira gromet dupla fina','TORNOZELEIRA',30,2,1,null,5,'2019-07-01',11.64,35,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Bracelete chapa meia cana grossa','BRACELETE',8,2,2,null,12,'2019-07-01',33.67,105,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Bracelete chapa reta grossa','BRACELETE',8,2,3,null,11,'2019-07-01',32.98,100,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Corrente laminada','CORRENTE',45,3,1,null,5,'2019-06-08',68.72,205,'Teste',true); 
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Anel de tartaruga','ANEL',null,null,null,17,10,'2019-06-08',23.87,70,'Anel envelhecido',true);	
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira 3 em 1 grossa','PULSEIRA',23,5,2,null,17,'2019-06-08',89.34,270,'Pulseira com chanfro',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira de Pandora','PULSEIRA',23,3,3,null,10,'2019-06-08',99.84,300,'Pulseira da Eternity',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira rabo de rato media','PULSEIRA',23,2,2,null,15,'2019-07-01',37.74,120,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pulseira de bolinha pequena','PULSEIRA',22,1,1,null,7,'2019-07-01',12.47,40,'Bolinhas ocas',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Corrente peruana','CORRENTE',50,15,15,null,120,'2019-07-01',212.74,650,'Pulseira envelhecida',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Pingente rabo de baleia','PINGENTE',5,4,1,null,4,'2019-07-01',21.04,65,'Lisa',true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Tornozeleira gromet dupla fina','TORNOZELEIRA',30,2,1,null,5,'2019-07-01',11.64,35,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Brinco de bola pequena','BRINCO',1,1,1,null,2,'2019-07-01',2.67,10,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Brinco de bola media','BRINCO',3,3,3,null,5,'2019-07-01',13.49,40,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Bracelete chapa meia cana grossa','BRACELETE',8,2,2,null,12,'2019-07-01',33.67,105,null,true);
INSERT INTO produto (nome,tipo,comprimento,largura,espessura,numero,peso,data_compra,valor_compra,valor_venda,observacao, vendido) values ('Bracelete chapa reta grossa','BRACELETE',8,2,3,null,11,'2019-07-01',32.98,100,null,true);

  