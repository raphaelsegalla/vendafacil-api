CREATE TABLE produtosvenda (
	codigo_venda BIGINT(20) NOT NULL,
	codigo_produto BIGINT(20) NOT NULL,
	FOREIGN KEY (codigo_venda) REFERENCES venda(codigo) ON DELETE CASCADE,
	FOREIGN KEY (codigo_produto) REFERENCES produto(codigo) ON DELETE CASCADE,
	UNIQUE (codigo_venda, codigo_produto)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;