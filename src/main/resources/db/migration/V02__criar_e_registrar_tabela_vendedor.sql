CREATE TABLE vendedor (
	codigo BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL,
	email VARCHAR(50) NOT NULL,
	cpf VARCHAR(20) NOT NULL,
	telefone VARCHAR(30) NOT NULL,
	celular VARCHAR(30) NOT NULL,
	logradouro VARCHAR(100),
	numero VARCHAR(30),
	complemento VARCHAR(30),
	bairro VARCHAR(30),
	cep VARCHAR(30),
	cidade VARCHAR(30),
	estado VARCHAR(30),
	ativo BOOLEAN NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO vendedor 
	(nome,email,cpf,telefone,celular,logradouro,numero,complemento,bairro,cep,cidade,estado,ativo) 
	values 
	('Raphael Segalla','raphael.segalla@email.com','12332145678','(12) 30116677','(12) 996586115', 'Rua do Abacaxi', '10', null, 'Brasil', '38.400-121', 'Uberlândia', 'MG', true);
	
INSERT INTO vendedor 
	(nome,email,cpf,telefone,celular,logradouro,numero,complemento,bairro,cep,cidade,estado,ativo) 
	values 
	('Nivaldo Segalla','Nivaldo.segalla@email.com','76545623412','(12) 39330987','(12) 988776655', 'Rua Euclides Miragaia', '789', 'apto.23', 'Brasil', '12.236-125', 'São José dos Campos', 'SP', true);	
	