package com.projeto.vendafacil.api.resource;

import java.math.BigDecimal;
import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.model.ProdutosVenda;
import com.projeto.vendafacil.api.model.Venda;
import com.projeto.vendafacil.api.repository.ProdutoRepository;
import com.projeto.vendafacil.api.repository.ProdutosVendaRepository;
import com.projeto.vendafacil.api.repository.VendaRepository;

@RestController
@RequestMapping("/produtosvenda")
public class ProdutosVendaResource {
	
	@Autowired
	private ProdutosVendaRepository produtosVendaRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private VendaRepository vendaRepository;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PRODUTO') and #oauth2.hasScope('read')")
	public List<ProdutosVenda> listar() {
		return produtosVendaRepository.findAll();
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PRODUTO') and #oauth2.hasScope('write')")
	public ResponseEntity<ProdutosVenda> adicionarProduto( @RequestBody ProdutosVenda produtosVenda, HttpServletResponse response) {
		ProdutosVenda produtosVendaSalvo = produtosVendaRepository.save(produtosVenda);
		
		Optional<Produto> produtoSalvo = produtoRepository.findById(produtosVendaSalvo.getCodigo().getProduto().getCodigo());
		produtoSalvo.get().setVendido(false);
		produtoRepository.save(produtoSalvo.get());
		
		
		Optional<Venda> vendaSalva = vendaRepository.findById(produtosVendaSalvo.getCodigo().getVenda().getCodigo());
		BigDecimal soma = vendaSalva.get().getValorVenda().add(produtoSalvo.get().getValorVenda());
		vendaSalva.get().setValorVenda(soma);
		vendaRepository.save(vendaSalva.get());
		
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
				  .buildAndExpand(produtosVendaSalvo.getCodigo().getVenda().getCodigo()).toUri();
		
		return ResponseEntity.created(uri).body(produtosVendaSalvo);
	}	 
}
