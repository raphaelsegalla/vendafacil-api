package com.projeto.vendafacil.api.resource.dto;

import java.util.List;

import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.model.Venda;

import lombok.Getter;

@Getter
public class VendaDto {
	
	private List<Produto> produtos;
	
	public VendaDto(Venda venda) {
		this.produtos = venda.getProdutos();
	}
	
}
