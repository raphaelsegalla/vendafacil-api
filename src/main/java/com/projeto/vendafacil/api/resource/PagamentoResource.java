package com.projeto.vendafacil.api.resource;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.vendafacil.api.event.RecursoCriadoEvent;
import com.projeto.vendafacil.api.model.Pagamento;
import com.projeto.vendafacil.api.model.Venda;
import com.projeto.vendafacil.api.repository.PagamentoRepository;
import com.projeto.vendafacil.api.repository.VendaRepository;

@RestController
@RequestMapping("/pagamentos")
public class PagamentoResource {

	@Autowired
	private PagamentoRepository pagamentoRepository;
	
	@Autowired
	private VendaRepository vendaRepository;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PAGAMENTO') and #oauth2.hasScope('read')")
	public List<Pagamento> listar() {
		return pagamentoRepository.findAll();
	}
	
	@GetMapping("/{codigo}") 
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_PAGAMENTO') and #oauth2.hasScope('read')")
	public ResponseEntity<Pagamento> buscarPeloCodigo(@PathVariable Long codigo) { 
		Optional<Pagamento> pagamento = pagamentoRepository.findById(codigo); 
		return pagamento.isPresent() ? ResponseEntity.ok(pagamento.get()) : ResponseEntity.notFound().build() ; 
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PAGAMENTO') and #oauth2.hasScope('write')")
	public ResponseEntity<Pagamento> adicionarPagamentoEntrada(@RequestBody Pagamento pagamento, HttpServletResponse response) {
		Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, pagamentoSalvo.getCodigo()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(pagamentoSalvo);
	}
	
	@PostMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_PAGAMENTO') and #oauth2.hasScope('write')")
	public ResponseEntity<Pagamento> adicionarPagamentoNaVenda(@RequestBody Pagamento pagamento,@PathVariable Long codigo, HttpServletResponse response) {
		Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);
		Optional<Venda> vendaSalva = vendaRepository.findById(codigo);
		BigDecimal valor = new BigDecimal(vendaSalva.get().getValorPago().toString());
		vendaSalva.get().setValorPago(valor.add(pagamentoSalvo.getValorPagamento()));
		vendaSalva.get().getPagamentos().add(pagamentoSalvo);
		
		vendaRepository.save(vendaSalva.get());
		
		publisher.publishEvent(new RecursoCriadoEvent(this, response, pagamentoSalvo.getCodigo()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(pagamentoSalvo);
	}
}
