package com.projeto.vendafacil.api.resource.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.model.TipoProduto;

import lombok.Getter;

@Getter
public class ProdutoDto {
	
	private Long codigo;
	private String nome;
	private TipoProduto tipo;
	private Double comprimento;
	private Double largura;
	private Double espessura;
	private Integer numero;
	private Double peso;
	private BigDecimal valorVenda;
	private Boolean vendido;
	
	public ProdutoDto(Produto produto) {
		this.codigo = produto.getCodigo();
		this.nome = produto.getNome();
		this.tipo = produto.getTipo();
		this.comprimento = produto.getComprimento();
		this.largura = produto.getLargura();
		this.espessura = produto.getEspessura();
		this.numero = produto.getNumero();
		this.peso = produto.getPeso();
		this.valorVenda = produto.getValorVenda();
		this.vendido = produto.getVendido();
	}
	
	public static List<ProdutoDto> converter(List<Produto> produtos) {
	
		return produtos.stream().map(ProdutoDto::new).collect(Collectors.toList());
	}

}
