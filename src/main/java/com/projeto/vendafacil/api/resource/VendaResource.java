package com.projeto.vendafacil.api.resource;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.vendafacil.api.event.RecursoCriadoEvent;
import com.projeto.vendafacil.api.model.Pagamento;
import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.model.Venda;
import com.projeto.vendafacil.api.repository.VendaRepository;
import com.projeto.vendafacil.api.repository.filter.VendaFilter;
import com.projeto.vendafacil.api.repository.projection.ResumoVenda;
import com.projeto.vendafacil.api.resource.dto.ProdutoDto;
import com.projeto.vendafacil.api.service.VendaService;

@RestController
@RequestMapping("/vendas")
public class VendaResource {
	
	@Autowired
	private VendaRepository vendaRepository;
	
	@Autowired
	private VendaService vendaService;
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDA') and #oauth2.hasScope('read')")
	public Page<Venda> pesquisar(VendaFilter vendaFilter, Pageable pageable) {
		return vendaRepository.filtrar(vendaFilter, pageable);
	}
	
	@GetMapping(params = "resumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDA') and #oauth2.hasScope('read')")
	public Page<ResumoVenda> resumir(VendaFilter vendaFilter, Pageable pageable) {
		return vendaRepository.resumir(vendaFilter, pageable);
	}
	
	@GetMapping("/{codigo}") 
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDA') and #oauth2.hasScope('read')")
	public ResponseEntity<Venda> buscarPeloCodigo(@PathVariable Long codigo) { 
		Optional<Venda> venda = vendaRepository.findById(codigo); 
		return venda.isPresent() ? ResponseEntity.ok(venda.get()) : ResponseEntity.notFound().build() ; 
	}
	
	@GetMapping("/{codigo}/produtos")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDA') and #oauth2.hasScope('read')")
	public ResponseEntity<List<Produto>> buscarProdutosPeloCodigo(@PathVariable Long codigo) { 
		List<Produto> produtos = vendaRepository.buscarProdutosDavenda(codigo); 
		return produtos != null ? ResponseEntity.ok(produtos) : ResponseEntity.notFound().build() ; 
	}
	
	@GetMapping("/{codigo}/pagamentos")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDA') and #oauth2.hasScope('read')")
	public ResponseEntity<List<Pagamento>> buscarPagamentosPeloCodigo(@PathVariable Long codigo) { 
		List<Pagamento> pagamentos = vendaRepository.buscarPagamentosDaVenda(codigo);
		return pagamentos != null ? ResponseEntity.ok(pagamentos) : ResponseEntity.notFound().build() ; 
	}
	
	@GetMapping("/{codigo}/produtosresumo")
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDA') and #oauth2.hasScope('read')")
	public ResponseEntity<List<ProdutoDto>> buscarProdutosPeloCodigoDaVendaResumo(@PathVariable Long codigo) { 
		List<Produto> produtos = vendaRepository.buscarProdutosDavenda(codigo); 
		List<ProdutoDto> produtosDto = ProdutoDto.converter(produtos);
		return produtosDto != null ? ResponseEntity.ok(produtosDto) : ResponseEntity.notFound().build() ; 
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_VENDA') and #oauth2.hasScope('write')")
	public ResponseEntity<Venda> cadastrarVenda(@Valid @RequestBody Venda venda, HttpServletResponse response) {
		Venda vendaSalva = vendaService.salvarVenda(venda);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, vendaSalva.getCodigo()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(vendaSalva);
	}
	
	@DeleteMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_REMOVER_VENDA') and #oauth2.hasScope('write')")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		vendaRepository.deleteById(codigo);
	}
}
