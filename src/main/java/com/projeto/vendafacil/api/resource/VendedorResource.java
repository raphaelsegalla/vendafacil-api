package com.projeto.vendafacil.api.resource;

import java.util.Optional;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.vendafacil.api.event.RecursoCriadoEvent;
import com.projeto.vendafacil.api.model.Vendedor;
import com.projeto.vendafacil.api.repository.VendedorRepository;
import com.projeto.vendafacil.api.repository.filter.VendedorFilter;
import com.projeto.vendafacil.api.service.VendedorService;


@RestController
@RequestMapping("/vendedores")
public class VendedorResource {
	
	@Autowired
	private VendedorRepository vendedorRepository;
	
	@Autowired
	private VendedorService vendedorService; 
	
	@Autowired
	private ApplicationEventPublisher publisher;
	
	@GetMapping
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDEDOR') and #oauth2.hasScope('read')")
	public Page<Vendedor> listar(VendedorFilter vendedorFilter, Pageable pageable) {
		return vendedorRepository.filtrar(vendedorFilter, pageable);
	}
	
	@GetMapping("/{codigo}") 
	@PreAuthorize("hasAuthority('ROLE_PESQUISAR_VENDEDOR') and #oauth2.hasScope('read')")
	public ResponseEntity<Vendedor> buscarPeloCodigo(@PathVariable Long codigo) { 
		Optional<Vendedor> vendedor = vendedorRepository.findById(codigo); 
		return vendedor.isPresent() ? ResponseEntity.ok(vendedor.get()) : ResponseEntity.notFound().build() ; 
	}
	
	@PostMapping
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_VENDEDOR') and #oauth2.hasScope('write')")
	public ResponseEntity<Vendedor> cadastrar(@Valid @RequestBody Vendedor vendedor, HttpServletResponse response) {
		Vendedor vendedorSalvo = vendedorRepository.save(vendedor);
		publisher.publishEvent(new RecursoCriadoEvent(this, response, vendedorSalvo.getCodigo()));
		
		return ResponseEntity.status(HttpStatus.CREATED).body(vendedorSalvo);
	}
	
	@DeleteMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_REMOVER_VENDEDOR') and #oauth2.hasScope('write')")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void remover(@PathVariable Long codigo) {
		vendedorRepository.deleteById(codigo);
	}
	
	@PutMapping("/{codigo}")
	@PreAuthorize("hasAuthority('ROLE_CADASTRAR_VENDEDOR') and #oauth2.hasScope('write')")
	public ResponseEntity<Vendedor> atualizar(@PathVariable Long codigo, @Valid @RequestBody Vendedor vendedor) {
		Vendedor vendedorSalvo = vendedorService.atualizar(codigo, vendedor);
		return ResponseEntity.ok(vendedorSalvo);
	}

}
