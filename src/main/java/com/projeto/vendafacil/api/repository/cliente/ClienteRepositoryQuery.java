package com.projeto.vendafacil.api.repository.cliente;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.projeto.vendafacil.api.model.Cliente;
import com.projeto.vendafacil.api.repository.filter.ClienteFilter;

public interface ClienteRepositoryQuery {
	
	public Page<Cliente> filtrar(ClienteFilter clienteFilter, Pageable pageable);

}
