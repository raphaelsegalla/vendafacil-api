package com.projeto.vendafacil.api.repository.filter;

import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import com.projeto.vendafacil.api.model.Vendedor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VendaFilter {

	private Vendedor vendedor;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataVendaDe;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataVendaAte;

}
