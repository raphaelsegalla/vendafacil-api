package com.projeto.vendafacil.api.repository.vendedor;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.projeto.vendafacil.api.model.Vendedor;
import com.projeto.vendafacil.api.repository.filter.VendedorFilter;

public interface VendedorRepositoryQuery {
	
	public Page<Vendedor> filtrar(VendedorFilter vendedorFilter, Pageable pageable);

}
