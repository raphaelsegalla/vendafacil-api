package com.projeto.vendafacil.api.repository.pagamento;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.projeto.vendafacil.api.model.Pagamento;
import com.projeto.vendafacil.api.repository.filter.PagamentoFilter;

public interface PagamentoRepositoryQuery {
	
	public Page<Pagamento> filtrar(PagamentoFilter pagamentoFilter, Pageable pageable);

}
