package com.projeto.vendafacil.api.repository.venda;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.projeto.vendafacil.api.model.Cliente_;
import com.projeto.vendafacil.api.model.Venda;
import com.projeto.vendafacil.api.model.Venda_;
import com.projeto.vendafacil.api.model.Vendedor_;
import com.projeto.vendafacil.api.repository.filter.VendaFilter;
import com.projeto.vendafacil.api.repository.projection.ResumoVenda;

public class VendaRepositoryImpl implements VendaRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Venda> filtrar(VendaFilter vendaFilter, Pageable pageable) {
		
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Venda> criteria = builder.createQuery(Venda.class);
		Root<Venda> root = criteria.from(Venda.class);
		
		Predicate[] predicates = criarRestricoes(vendaFilter, builder, root);
		criteria.where(predicates).distinct(true);
		
		TypedQuery<Venda> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(vendaFilter));
	}
	
	@Override
	public Page<ResumoVenda> resumir(VendaFilter vendaFilter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<ResumoVenda> criteria = builder.createQuery(ResumoVenda.class);
		Root<Venda> root = criteria.from(Venda.class);
		
		criteria.select(builder.construct(ResumoVenda.class
				, root.get(Venda_.codigo)
				, root.get(Venda_.dataVenda)
				, root.get(Venda_.dataPagamento)
				, root.get(Venda_.vendedor).get(Vendedor_.nome)
				, root.get(Venda_.cliente).get(Cliente_.nome)
				, root.get(Venda_.valorVenda)
				, root.get(Venda_.valorPago)));
		
		Predicate[] predicates = criarRestricoes(vendaFilter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<ResumoVenda> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(vendaFilter));
	}
	
	private Predicate[] criarRestricoes(VendaFilter vendaFilter, CriteriaBuilder builder, Root<Venda> root) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(vendaFilter.getVendedor()) ) {
			predicates.add(builder.equal(root.get(Venda_.vendedor), vendaFilter.getVendedor()));
		}
		
		if (vendaFilter.getDataVendaDe() != null) {
			predicates.add(
					builder.greaterThanOrEqualTo(root.get(Venda_.dataVenda), vendaFilter.getDataVendaDe()));
		}
		
		if (vendaFilter.getDataVendaAte() != null) {
			predicates.add(
					builder.lessThanOrEqualTo(root.get(Venda_.dataVenda), vendaFilter.getDataVendaAte()));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(VendaFilter vendaFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Venda> root = criteria.from(Venda.class);
		
		Predicate[] predicates = criarRestricoes(vendaFilter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}
}
