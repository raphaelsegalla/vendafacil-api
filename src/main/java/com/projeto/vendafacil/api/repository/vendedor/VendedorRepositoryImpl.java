package com.projeto.vendafacil.api.repository.vendedor;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.projeto.vendafacil.api.model.Endereco;
import com.projeto.vendafacil.api.model.Endereco_;
import com.projeto.vendafacil.api.model.Vendedor;
import com.projeto.vendafacil.api.model.Vendedor_;
import com.projeto.vendafacil.api.repository.filter.VendedorFilter;

public class VendedorRepositoryImpl implements VendedorRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Vendedor> filtrar(VendedorFilter vendedorFilter, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Vendedor> criteria = builder.createQuery(Vendedor.class);
		Root<Vendedor> root = criteria.from(Vendedor.class);
		Join<Vendedor, Endereco> vendedorEnd = root.join(Vendedor_.endereco);
		
		Predicate[] predicates = criarRestricoes(vendedorFilter, builder, root, vendedorEnd);
		criteria.where(predicates).distinct(true);
		
		TypedQuery<Vendedor> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(vendedorFilter));
	}
	
	private Predicate[] criarRestricoes(VendedorFilter vendedorFilter, CriteriaBuilder builder, Root<Vendedor> root, Join<Vendedor, Endereco> vendedorEnd) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(vendedorFilter.getNome()) ) {
			predicates.add(builder.like(builder.lower(root.get(Vendedor_.nome)), "%" + vendedorFilter.getNome().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(vendedorFilter.getEmail()) ) {
			predicates.add(builder.like(builder.lower(root.get(Vendedor_.email)), "%" + vendedorFilter.getEmail().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(vendedorFilter.getTelefone()) ) {
			predicates.add(builder.like(builder.lower(root.get(Vendedor_.telefone)), "%" + vendedorFilter.getTelefone().toLowerCase() + "%"));
		}
		
		if (vendedorFilter.getLogradouro() != null ) {
			predicates.add(builder.like(builder.lower(vendedorEnd.get(Endereco_.logradouro)), "%" + vendedorFilter.getLogradouro() + "%"));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<Vendedor> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(VendedorFilter vendedorFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Vendedor> root = criteria.from(Vendedor.class);
		Join<Vendedor, Endereco> vendedorEnd = root.join(Vendedor_.endereco);
		
		Predicate[] predicates = criarRestricoes(vendedorFilter, builder, root, vendedorEnd);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
