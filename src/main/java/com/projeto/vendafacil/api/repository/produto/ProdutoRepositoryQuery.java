package com.projeto.vendafacil.api.repository.produto;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.repository.filter.ProdutoFilter;
import com.projeto.vendafacil.api.repository.projection.ResumoProduto;

public interface ProdutoRepositoryQuery {
	
	public Page<Produto> filtrar(ProdutoFilter produtoFilter, Pageable pageable);
	public Page<ResumoProduto> resumir(ProdutoFilter produtoFilter, Pageable pageable);

}
