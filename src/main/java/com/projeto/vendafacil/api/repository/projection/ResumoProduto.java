package com.projeto.vendafacil.api.repository.projection;

import java.math.BigDecimal;

import com.projeto.vendafacil.api.model.TipoProduto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResumoProduto {

	private Long codigo;
	private String nome;
	private TipoProduto tipo;
	private Double comprimento;
	private Double largura;
	private Double espessura;
	private Integer numero;
	private Double peso;
	private BigDecimal valorVenda;
	private Boolean vendido;
	
}
