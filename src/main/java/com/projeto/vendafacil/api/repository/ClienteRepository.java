package com.projeto.vendafacil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.vendafacil.api.model.Cliente;
import com.projeto.vendafacil.api.repository.cliente.ClienteRepositoryQuery;

public interface ClienteRepository extends JpaRepository<Cliente, Long>, ClienteRepositoryQuery{

}
