package com.projeto.vendafacil.api.repository.venda;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.projeto.vendafacil.api.model.Venda;
import com.projeto.vendafacil.api.repository.filter.VendaFilter;
import com.projeto.vendafacil.api.repository.projection.ResumoVenda;

public interface VendaRepositoryQuery {
	
	public Page<Venda> filtrar(VendaFilter vendaFilter, Pageable pageable);
	public Page<ResumoVenda> resumir(VendaFilter vendaFilter, Pageable pageable);

}
