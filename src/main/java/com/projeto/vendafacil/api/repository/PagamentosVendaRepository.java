package com.projeto.vendafacil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.vendafacil.api.model.PagamentosVenda;
import com.projeto.vendafacil.api.model.PagamentosVendaId;

public interface PagamentosVendaRepository extends JpaRepository<PagamentosVenda, PagamentosVendaId>{

}
