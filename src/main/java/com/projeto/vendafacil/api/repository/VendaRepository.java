package com.projeto.vendafacil.api.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.projeto.vendafacil.api.model.Pagamento;
import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.model.Venda;
import com.projeto.vendafacil.api.repository.venda.VendaRepositoryQuery;

public interface VendaRepository extends JpaRepository<Venda, Long>, VendaRepositoryQuery{

	@Query("select produtos FROM Venda v where v.codigo =:codigo")
	public List<Produto> buscarProdutosDavenda(@Param("codigo") Long codigo);
	
	@Query("select pagamentos FROM Venda v where v.codigo =:codigo")
	public List<Pagamento> buscarPagamentosDaVenda(@Param("codigo") Long codigo);

}
