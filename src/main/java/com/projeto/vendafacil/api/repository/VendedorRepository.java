package com.projeto.vendafacil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.vendafacil.api.model.Vendedor;
import com.projeto.vendafacil.api.repository.vendedor.VendedorRepositoryQuery;

public interface VendedorRepository extends JpaRepository<Vendedor, Long>, VendedorRepositoryQuery{

}
