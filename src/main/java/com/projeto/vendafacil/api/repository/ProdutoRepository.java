package com.projeto.vendafacil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.repository.produto.ProdutoRepositoryQuery;

public interface ProdutoRepository extends JpaRepository<Produto, Long>, ProdutoRepositoryQuery{

}
