package com.projeto.vendafacil.api.repository.filter;

import java.time.LocalDate;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import org.springframework.format.annotation.DateTimeFormat;

import com.projeto.vendafacil.api.model.TipoPagamento;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PagamentoFilter {

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataPagamentoDe;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate dataPagamentoAte;
	
	@Enumerated(EnumType.STRING)
	private TipoPagamento tipoPagamento;
	
}
