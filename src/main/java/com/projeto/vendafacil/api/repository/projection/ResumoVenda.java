package com.projeto.vendafacil.api.repository.projection;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResumoVenda {
	
	private Long codigo;
	private LocalDate dataVenda;
	private LocalDate dataPagamento;
	private String vendedor;
	private String cliente;
	private BigDecimal valorVenda;
	private BigDecimal valorPago;

}
