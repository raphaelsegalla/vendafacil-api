package com.projeto.vendafacil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.vendafacil.api.model.Pagamento;

public interface PagamentoRepository extends JpaRepository<Pagamento, Long> {

}
