package com.projeto.vendafacil.api.repository.cliente;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import com.projeto.vendafacil.api.model.Cliente;
import com.projeto.vendafacil.api.model.Cliente_;
import com.projeto.vendafacil.api.model.Endereco;
import com.projeto.vendafacil.api.model.Endereco_;
import com.projeto.vendafacil.api.repository.filter.ClienteFilter;

public class ClienteRepositoryImpl implements ClienteRepositoryQuery{
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public Page<Cliente> filtrar(ClienteFilter clienteFilter, Pageable pageable) {

		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Cliente> criteria = builder.createQuery(Cliente.class);
		Root<Cliente> root = criteria.from(Cliente.class);
		Join<Cliente, Endereco> clienteEnd = root.join(Cliente_.endereco);
		
		Predicate[] predicates = criarRestricoes(clienteFilter, builder, root, clienteEnd);
		criteria.where(predicates).distinct(true);
		
		TypedQuery<Cliente> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(clienteFilter));
	}
	
	private Predicate[] criarRestricoes(ClienteFilter clienteFilter, CriteriaBuilder builder, Root<Cliente> root, Join<Cliente, Endereco> clienteEnd) {
		List<Predicate> predicates = new ArrayList<>();
		
		if (!StringUtils.isEmpty(clienteFilter.getNome()) ) {
			predicates.add(builder.like(builder.lower(root.get(Cliente_.nome)), "%" + clienteFilter.getNome().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(clienteFilter.getEmail()) ) {
			predicates.add(builder.like(builder.lower(root.get(Cliente_.email)), "%" + clienteFilter.getEmail().toLowerCase() + "%"));
		}
		
		if (!StringUtils.isEmpty(clienteFilter.getTelefone()) ) {
			predicates.add(builder.like(builder.lower(root.get(Cliente_.telefone)), "%" + clienteFilter.getTelefone().toLowerCase() + "%"));
		}
		
		if (clienteFilter.getLogradouro() != null ) {
			predicates.add(builder.like(builder.lower(clienteEnd.get(Endereco_.logradouro)), "%" + clienteFilter.getLogradouro() + "%"));
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<Cliente> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(ClienteFilter clienteFilter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<Cliente> root = criteria.from(Cliente.class);
		Join<Cliente, Endereco> clienteEnd = root.join(Cliente_.endereco);
		
		Predicate[] predicates = criarRestricoes(clienteFilter, builder, root, clienteEnd);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}
