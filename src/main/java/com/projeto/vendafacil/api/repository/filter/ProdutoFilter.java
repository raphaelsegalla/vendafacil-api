package com.projeto.vendafacil.api.repository.filter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.projeto.vendafacil.api.model.TipoProduto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProdutoFilter {
	
	private String nome;
	
	@Enumerated(EnumType.STRING)
	private TipoProduto tipo;

	private Boolean vendido;
}
