package com.projeto.vendafacil.api.repository.filter;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VendedorFilter {

	private String nome;
	
	private String email;
	
	private String telefone;
	
	private String logradouro;
	
}
