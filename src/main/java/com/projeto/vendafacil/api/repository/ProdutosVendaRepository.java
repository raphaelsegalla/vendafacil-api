package com.projeto.vendafacil.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.vendafacil.api.model.ProdutosVenda;
import com.projeto.vendafacil.api.model.ProdutosVendaId;

public interface ProdutosVendaRepository extends JpaRepository<ProdutosVenda, ProdutosVendaId>{

}
