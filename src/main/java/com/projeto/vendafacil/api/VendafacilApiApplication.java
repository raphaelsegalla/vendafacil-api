package com.projeto.vendafacil.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import com.projeto.vendafacil.api.config.property.VendaFacilApiProperty;

@SpringBootApplication
@EnableConfigurationProperties(VendaFacilApiProperty.class)
public class VendafacilApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendafacilApiApplication.class, args);
	}

}
