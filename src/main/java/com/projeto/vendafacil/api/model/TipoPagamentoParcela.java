package com.projeto.vendafacil.api.model;

public enum TipoPagamentoParcela {
	TOTAL,
	ENTRADA,
	PARCELA_01,
	PARCELA_02,
	PARCELA_03,
	PARCELA_04,
	PARCELA_05,
	PARCELA_06,
	PARCELA_07,
	PARCELA_08,
	PARCELA_09,
	PARCELA_10,
	PARCELA_11,
	PARCELA_12
}
