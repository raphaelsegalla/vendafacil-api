package com.projeto.vendafacil.api.model;

public enum TipoProduto {
	CORRENTE,
	PULSEIRA,
	ANEL,
	PINGENTE,
	BRINCO,
	BRACELETE,
	TORNOZELEIRA
}
