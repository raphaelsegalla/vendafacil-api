package com.projeto.vendafacil.api.model;

import java.io.Serializable;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pagamentosvenda")
public class PagamentosVenda implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@EmbeddedId
	private PagamentosVendaId codigo;

}
