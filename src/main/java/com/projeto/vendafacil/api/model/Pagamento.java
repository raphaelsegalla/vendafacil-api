package com.projeto.vendafacil.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "pagamento")
public class Pagamento {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@NotNull
	@Column(name = "data_pagamento")
	private LocalDate dataPagamento; // = LocalDate.now();
	
	@NotNull
	@Column(name = "valor_pagamento")
	private BigDecimal valorPagamento;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_pagamento")
	private TipoPagamento tipoPagamento;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo_parcela")
	private TipoPagamentoParcela tipoParcela;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JsonBackReference
	@JoinTable(name = "pagamentosvenda",
		joinColumns = @JoinColumn(name = "codigo_pagamento", referencedColumnName = "codigo"),
		inverseJoinColumns = @JoinColumn(name = "codigo_venda", referencedColumnName = "codigo"))
	private List<Venda> vendas;
	
	private String observacao;
	
	@NotNull
	@Column(name = "total_pago")
	private Boolean totalPago;
	
}
