package com.projeto.vendafacil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Vendedor.class)
public abstract class Vendedor_ {

	public static volatile SingularAttribute<Vendedor, Long> codigo;
	public static volatile SingularAttribute<Vendedor, String> telefone;
	public static volatile SingularAttribute<Vendedor, Boolean> ativo;
	public static volatile SingularAttribute<Vendedor, Endereco> endereco;
	public static volatile SingularAttribute<Vendedor, String> cpf;
	public static volatile SingularAttribute<Vendedor, String> celular;
	public static volatile SingularAttribute<Vendedor, String> nome;
	public static volatile SingularAttribute<Vendedor, String> email;

	public static final String CODIGO = "codigo";
	public static final String TELEFONE = "telefone";
	public static final String ATIVO = "ativo";
	public static final String ENDERECO = "endereco";
	public static final String CPF = "cpf";
	public static final String CELULAR = "celular";
	public static final String NOME = "nome";
	public static final String EMAIL = "email";

}

