package com.projeto.vendafacil.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "venda")
public class Venda {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@NotNull
	@Column(name = "data_venda")
	private LocalDate dataVenda;
	
	@NotNull
	@Column(name = "data_pagamento")
	private LocalDate dataPagamento;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_vendedor")
	private Vendedor vendedor;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "codigo_cliente")
	private Cliente cliente;
	
	@Column(name = "valor_venda")
	private BigDecimal valorVenda;
	
	@Column(name = "valor_pago")
	private BigDecimal valorPago;
	
	@ManyToMany
	@JoinTable(name = "produtosvenda", 
		joinColumns = @JoinColumn(name = "codigo_venda", referencedColumnName = "codigo"), 
		inverseJoinColumns = @JoinColumn(name = "codigo_produto", referencedColumnName = "codigo"))
	private List<Produto> produtos;
	
	@ManyToMany
	@JoinTable(name = "pagamentosvenda", 
		joinColumns = @JoinColumn(name = "codigo_venda", referencedColumnName = "codigo"), 
		inverseJoinColumns = @JoinColumn(name = "codigo_pagamento", referencedColumnName = "codigo"))
	private List<Pagamento> pagamentos;
}