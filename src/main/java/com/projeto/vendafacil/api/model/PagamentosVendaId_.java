package com.projeto.vendafacil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PagamentosVendaId.class)
public abstract class PagamentosVendaId_ {

	public static volatile SingularAttribute<PagamentosVendaId, Venda> venda;
	public static volatile SingularAttribute<PagamentosVendaId, Pagamento> pagamento;

	public static final String VENDA = "venda";
	public static final String PAGAMENTO = "pagamento";

}

