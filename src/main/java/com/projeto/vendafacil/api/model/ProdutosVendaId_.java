package com.projeto.vendafacil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProdutosVendaId.class)
public abstract class ProdutosVendaId_ {

	public static volatile SingularAttribute<ProdutosVendaId, Venda> venda;
	public static volatile SingularAttribute<ProdutosVendaId, Produto> produto;

	public static final String VENDA = "venda";
	public static final String PRODUTO = "produto";

}

