package com.projeto.vendafacil.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pagamento.class)
public abstract class Pagamento_ {

	public static volatile SingularAttribute<Pagamento, Long> codigo;
	public static volatile SingularAttribute<Pagamento, String> observacao;
	public static volatile SingularAttribute<Pagamento, LocalDate> dataPagamento;
	public static volatile SingularAttribute<Pagamento, TipoPagamentoParcela> tipoParcela;
	public static volatile SingularAttribute<Pagamento, Boolean> totalPago;
	public static volatile ListAttribute<Pagamento, Venda> vendas;
	public static volatile SingularAttribute<Pagamento, TipoPagamento> tipoPagamento;
	public static volatile SingularAttribute<Pagamento, BigDecimal> valorPagamento;

	public static final String CODIGO = "codigo";
	public static final String OBSERVACAO = "observacao";
	public static final String DATA_PAGAMENTO = "dataPagamento";
	public static final String TIPO_PARCELA = "tipoParcela";
	public static final String TOTAL_PAGO = "totalPago";
	public static final String VENDAS = "vendas";
	public static final String TIPO_PAGAMENTO = "tipoPagamento";
	public static final String VALOR_PAGAMENTO = "valorPagamento";

}

