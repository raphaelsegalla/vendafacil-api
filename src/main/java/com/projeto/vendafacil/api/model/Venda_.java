package com.projeto.vendafacil.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Venda.class)
public abstract class Venda_ {

	public static volatile SingularAttribute<Venda, Cliente> cliente;
	public static volatile SingularAttribute<Venda, Long> codigo;
	public static volatile SingularAttribute<Venda, Vendedor> vendedor;
	public static volatile SingularAttribute<Venda, LocalDate> dataVenda;
	public static volatile SingularAttribute<Venda, LocalDate> dataPagamento;
	public static volatile ListAttribute<Venda, Pagamento> pagamentos;
	public static volatile SingularAttribute<Venda, BigDecimal> valorVenda;
	public static volatile ListAttribute<Venda, Produto> produtos;
	public static volatile SingularAttribute<Venda, BigDecimal> valorPago;

	public static final String CLIENTE = "cliente";
	public static final String CODIGO = "codigo";
	public static final String VENDEDOR = "vendedor";
	public static final String DATA_VENDA = "dataVenda";
	public static final String DATA_PAGAMENTO = "dataPagamento";
	public static final String PAGAMENTOS = "pagamentos";
	public static final String VALOR_VENDA = "valorVenda";
	public static final String PRODUTOS = "produtos";
	public static final String VALOR_PAGO = "valorPago";

}

