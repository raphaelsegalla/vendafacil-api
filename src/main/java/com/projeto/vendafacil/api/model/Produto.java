package com.projeto.vendafacil.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "produto")
public class Produto {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long codigo;
	
	@NotNull
	private String nome;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	private TipoProduto tipo;
	
	private Double comprimento;
	
	private Double largura;
	
	private Double espessura;
	
	private Integer numero;
	
	private Double peso;
	
	@NotNull
	@Column(name = "data_compra")
	private LocalDate dataCompra;
	
	@NotNull
	@Column(name = "valor_compra")
	private BigDecimal valorCompra;
	
	@NotNull
	@Column(name = "valor_venda")
	private BigDecimal valorVenda;
	
	private String observacao;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JsonBackReference
	@JoinTable(name = "produtosvenda",
		joinColumns = @JoinColumn(name = "codigo_produto", referencedColumnName = "codigo"),
		inverseJoinColumns = @JoinColumn(name = "codigo_venda", referencedColumnName = "codigo"))
	private List<Venda> vendas;
	
	@NotNull
	private Boolean vendido;
	
}
