package com.projeto.vendafacil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ProdutosVenda.class)
public abstract class ProdutosVenda_ {

	public static volatile SingularAttribute<ProdutosVenda, ProdutosVendaId> codigo;

	public static final String CODIGO = "codigo";

}

