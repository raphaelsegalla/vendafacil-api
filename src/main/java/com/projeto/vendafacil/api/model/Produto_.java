package com.projeto.vendafacil.api.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Produto.class)
public abstract class Produto_ {

	public static volatile SingularAttribute<Produto, Long> codigo;
	public static volatile SingularAttribute<Produto, TipoProduto> tipo;
	public static volatile SingularAttribute<Produto, String> observacao;
	public static volatile SingularAttribute<Produto, Double> largura;
	public static volatile SingularAttribute<Produto, Integer> numero;
	public static volatile SingularAttribute<Produto, Double> espessura;
	public static volatile SingularAttribute<Produto, Double> peso;
	public static volatile SingularAttribute<Produto, BigDecimal> valorCompra;
	public static volatile ListAttribute<Produto, Venda> vendas;
	public static volatile SingularAttribute<Produto, String> nome;
	public static volatile SingularAttribute<Produto, LocalDate> dataCompra;
	public static volatile SingularAttribute<Produto, BigDecimal> valorVenda;
	public static volatile SingularAttribute<Produto, Double> comprimento;
	public static volatile SingularAttribute<Produto, Boolean> vendido;

	public static final String CODIGO = "codigo";
	public static final String TIPO = "tipo";
	public static final String OBSERVACAO = "observacao";
	public static final String LARGURA = "largura";
	public static final String NUMERO = "numero";
	public static final String ESPESSURA = "espessura";
	public static final String PESO = "peso";
	public static final String VALOR_COMPRA = "valorCompra";
	public static final String VENDAS = "vendas";
	public static final String NOME = "nome";
	public static final String DATA_COMPRA = "dataCompra";
	public static final String VALOR_VENDA = "valorVenda";
	public static final String COMPRIMENTO = "comprimento";
	public static final String VENDIDO = "vendido";

}

