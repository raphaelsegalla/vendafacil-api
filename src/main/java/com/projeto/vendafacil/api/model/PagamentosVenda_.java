package com.projeto.vendafacil.api.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PagamentosVenda.class)
public abstract class PagamentosVenda_ {

	public static volatile SingularAttribute<PagamentosVenda, PagamentosVendaId> codigo;

	public static final String CODIGO = "codigo";

}

