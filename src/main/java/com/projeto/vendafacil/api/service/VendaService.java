package com.projeto.vendafacil.api.service;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.model.Venda;
import com.projeto.vendafacil.api.repository.ProdutoRepository;
import com.projeto.vendafacil.api.repository.VendaRepository;
import com.projeto.vendafacil.api.service.exception.ProdutoInexistenteOuInativaException;

@Service
public class VendaService {
	
	@Autowired
	private VendaRepository vendaRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	public Venda salvarVenda(@Valid Venda venda) {
		List<Produto> produtos = venda.getProdutos();
		for (Produto produto : produtos) {
			Optional<Produto> produtoSalvo = produtoRepository.findById(produto.getCodigo());
			if (produto == null || produtoSalvo.get().getVendido() == false) {
				throw new ProdutoInexistenteOuInativaException();
			}
			produtoSalvo.get().setVendido(false);
		}
		return vendaRepository.save(venda);
	}

}
