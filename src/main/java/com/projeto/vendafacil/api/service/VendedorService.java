package com.projeto.vendafacil.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.vendafacil.api.model.Vendedor;
import com.projeto.vendafacil.api.repository.VendedorRepository;

@Service
public class VendedorService {
	
	@Autowired
	VendedorRepository vendedorRepository;
	
	public Vendedor atualizar(Long codigo, Vendedor vendedor) {
		Optional<Vendedor> vendedorSalvo = vendedorRepository.findById(codigo);
		BeanUtils.copyProperties(vendedor, vendedorSalvo.get(), "codigo");
		return vendedorRepository.save(vendedorSalvo.get());	
	}

}
