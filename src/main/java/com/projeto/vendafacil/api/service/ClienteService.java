package com.projeto.vendafacil.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.vendafacil.api.model.Cliente;
import com.projeto.vendafacil.api.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRepository;
	
	public Cliente atualizar(Long codigo, Cliente cliente) {
		Optional<Cliente> clienteSalvo = clienteRepository.findById(codigo);
		BeanUtils.copyProperties(cliente, clienteSalvo.get(), "codigo");
		return clienteRepository.save(clienteSalvo.get());	
	}

}
