package com.projeto.vendafacil.api.service;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.vendafacil.api.model.Produto;
import com.projeto.vendafacil.api.repository.ProdutoRepository;

@Service
public class ProdutoService {
	
	@Autowired
	ProdutoRepository produtoRepository;
	
	public Produto atualizar(Long codigo, Produto produto) {
		Optional<Produto> produtoSalvo = produtoRepository.findById(codigo);
		BeanUtils.copyProperties(produto, produtoSalvo.get(), "codigo");
		return produtoRepository.save(produtoSalvo.get());	
	}

}
